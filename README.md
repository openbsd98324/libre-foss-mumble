#  Welcome to FOSS and Libre, Software 



a server for libre, free, foss, and free software talk.




## Installation under Linux


To install the mumble client:

````
sudo apt-get install mumble 

mumble 
````




## Connecting to Libre, FOSS

![](my-mumble-libre.org-usage.png)

![](my-mumble-libre.org.png)



## For eventual clients, using Android:

URL: 
https://play.google.com/store/apps/details?id=com.morlunk.mumbleclient.free&hl=en_US&gl=US

We will of course not recommend Google, Android, and play google.
We may recommend OpenBSD, FreeBSD, NetBSD, and Linux.


## GNU 


Please Visit a TeDx Video:

 https://audio-video.gnu.org/video/TEDxGE2014_Stallman05_LQ.webm



## Sharing FOSS Talks


Link with the mumble, easy "how-to":

https://postimg.cc/gallery/ZYPtXnh



## Source code

Mumble is free:

https://github.com/mumble-voip/mumble

"the mumble is not the best source code, ever, but we are working hard to bring SIP/VOIP, using linphone, asterisk, sip, sofia sip,.... Please join the dev if you would like."




## References

www.gnu.org

www.openbsd.org


